package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskShowByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskShowByIndexRequest(
            @Nullable final String token,
            @Nullable Integer index
    ) {
        super(token);
        this.index = index;
    }

}