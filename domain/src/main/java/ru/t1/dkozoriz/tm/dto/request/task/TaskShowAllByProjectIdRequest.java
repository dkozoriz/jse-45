package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskShowAllByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowAllByProjectIdRequest(
            @Nullable final String token,
            @Nullable String projectId
    ) {
        super(token);
        this.projectId = projectId;
    }

}